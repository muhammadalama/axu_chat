import 'package:flutter/material.dart';
import 'dart:math';

class ProfileScreen extends StatefulWidget {
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with TickerProviderStateMixin {
  TabController _headerTabController;
  TabController _tabController;

  List<TabText> _tabs;

  int _selectedIndex = 0;
  List<Color> _colors = [
    Colors.orangeAccent,
    Colors.redAccent,
    Colors.lightBlue,
  ];

  AnimationController _createTabAnimation() {
    return AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );
  }

  @override
  void initState() {
    super.initState();

    _tabs = [
      TabText(
        Icons.contact_phone,
        text: "Contact",
        animation: _createTabAnimation(),
      ),
      TabText(
        Icons.perm_media,
        text: "Media",
        animation: _createTabAnimation(),
      ),
      TabText(
        Icons.list,
        text: "Task",
        animation: _createTabAnimation(),
      ),
    ];

    _headerTabController = new TabController(vsync: this, length: 3);
    _tabController = new TabController(vsync: this, length: 3);
    _tabController.animation.addListener(() {
      //_tabAnimation.value = _tabController.animation.value;

      int index = _tabController.animation.value.round();
      if (!_headerTabController.indexIsChanging && index != _selectedIndex) {
        _headerTabController.animateTo(index,
            duration: Duration(milliseconds: 200));
        _selectedIndex = index;
      }
    });

    _headerTabController.addListener(_handleHeaderTabSelection);

    _tabs[_selectedIndex].animation.forward();
  }

  void _handleHeaderTabSelection() {
    int index = _headerTabController.index;
    if (_headerTabController.indexIsChanging) {
      _tabs[_selectedIndex].animation.reverse();
      _tabs[index].animation.forward();
      _tabController.animateTo(index, duration: Duration(milliseconds: 200));
    }

    _selectedIndex = index;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          splashColor: Colors.transparent, highlightColor: Colors.transparent),
      child: Scaffold(
        appBar: AppBar(
          title: Text("Profile"),
          bottom: PreferredSize(
            preferredSize: Size(0, 42),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 4.0),
              child: TabBar(
                controller: _headerTabController,
                isScrollable: true,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(21.0),
                  color: _colors[0],
                ),
                tabs: _tabs,
                onTap: (index) {},
              ),
            ),
          ),
        ),
        body: Container(
          child: TabBarView(
            controller: _tabController,
            children: <Widget>[
              Center(child: Icon(Icons.contact_phone)),
              Center(child: Icon(Icons.perm_media)),
              Center(child: Icon(Icons.list))
            ],
          ),
        ),
      ),
    );
  }
}

class TabText extends StatelessWidget {
  final String text;
  final IconData icon;
  final AnimationController animation;

  TabText(this.icon, {this.text, this.animation});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      height: 32.0,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(icon),
          SizeTransition(
            sizeFactor: CurvedAnimation(
              parent: animation,
              curve: Curves.linear,
            ),
            axis: Axis.horizontal,
            axisAlignment: 1.0,
            child: Container(
              margin: EdgeInsets.only(left: 8),
              child: Center(child: Text(text)),
            ),
          )
        ],
      ),
    );
  }
}
